# This is a template for [Stog](https://zoggy.github.io/stog/), using [Foundation framework](http://foundation.zurb.com)

It is inspired from templates available [here](http://foundation.zurb.com/templates.html)

## Usage

You can use the install script
```bash
$ ./install.sh <your stog folder>
```

**Warning: This will delete any preceding template**


### Customize

You can add your customization in files ```template/menubar.tmpl```, ```template/footer.tmpl```, ```template/side-panel.tmpl```, ```template/comments.tmpl```.     
You can also edit the css file named ```custom.css``` on the root directory. If
you want it to be used, you must set ```custum``` tag to true (or anything else not
empty)

The template use some special tags :

    stog:ads="true"
    stog:author="John Donym"
    stog:sidebar="active"
    stog:comm="true"
    banner="true"
        

You can give to all tag a local meaning by removing the stog prefix.
The tag ```ads``` allows you to display ads on your site when set to true.
The tag ```author``` explains who is the creator of the page.
The tag ```description``` is used to provide a description for the document,
without it, one is generated from `doc-intro`.
The tag ```banner``` is used  to get a banner, color would be customizable someday.

## Get help

Send an email to leowzukw@vmail.me.