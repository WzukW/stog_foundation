#!/bin/sh

# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

# =================================================================
# Tools

# Function to display warnings
warn()
{
    echo '\033[1;30;47m Warning: \033[0;32m\033[1;33m' $1 '\033[0m'
}

# Function to display error messages
error()
{
    echo '\033[1;30;47m Error: \033[0;32m\033[1;31m' $1 '\033[0m'
}

# Function to display informations
info()
{
        echo '\033[1;30;47m Info: \033[0;32m\033[1;34m' $1 '\033[0m'
}

# Function to display good things
good()
{
        echo '\033[1;30;47m [✔] \033[0;32m\033[1;32m' $1 '\033[0m'
}

# XXX Testing
echo Testing
warn "warn"
error "error"
info "info"
good "good"


# =================================================================
#                 Verify that a name is given
ARGS=1
E_NOFILE=65
E_FILEDOESNOTEXIST=66
E_NODOTSTOG=50

if [ $# -ne $ARGS ]  # Less arguments than necessary
then
    error "The script takes one arguments, the folder where your Stog sources are!"
    error "Usage: `basename $0` filename"
    exit $E_NOFILE
fi

if [ -d "$1" ]       # Verify that folder exist
then
    stog_folder=$1
else
    error "The file \"$1\" is not a folder or does not exist."
    exit $E_FILEDOESNOTEXIST
fi
# =================================================================
# Copy
#   - the content of $TEMPLATE in .stog/template, after deleting all
#   - the other files on the root of the stog folder
template=$PWD/templates/*
other=$PWD/other/*

# If folder exists, delete all
dot_stog_t=$stog_folder/.stog/templates
if [[ -d "$dot_stog_t" ]]; then
    warn "Removing all files in $dot_stog_t"
    current=$PWD
    cd $dot_stog_t

    # XXX
    ls

    rm *
    cd $current
else
    error "Folder $stog_folder does not contain any .stog/templates folder, exiting."
    warn "Do you want to create it?"
    read answer

    case $answer in
        "y" | "yes") info "Creating directory"; mkdir -p $dot_stog_t;;
        "n" | "no")  exit $E_NODOTSTOG;;
    esac
fi

# Copy
info "Copying 1/2"
cp -v $template $dot_stog_t
info "Copying 2/2"
cp -v $other $stog_folder

# Finished
good "Finished"
exit 0
