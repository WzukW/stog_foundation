#!/bin/sh

# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org/>

# Script to test the template

# Source folder
src=./mystog/
in=/tmp/stog/
out=/tmp/made/
# Packages, comma separated
pkg=

# If folder existing in /tmp, delete it
if [ -d $in ]       # Verify that folder exist
then
    rm -r $in
fi

# Ouput file must exist and must be reinitialised
if [ -d $out ]; then
    rm -r $out
fi

mkdir $in $out

# Copy files
cp -r ${src}* ${src}.stog $in

# Add files and overwrite first warning, we know we erase all
echo 'y\n' | ./install.sh $in

# Create config file to avoid error
echo \
'{"ignored":[],"documents":[".*\\\\.html$"],"not_documents":[],"follow_symlinks":true,"levels":[]}' \
> ${in}.stog/config

# Build the website
stog_cmd="stog --verbose 3 -d $out --local $in"
echo $stog_cmd
$stog_cmd
